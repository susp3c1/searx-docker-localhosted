# searx-docker-localhosted

Local hosted version of searx using docker. Dont use it on a server :wink:

Fork of [searx-docker](https://github.com/searx/searx-docker)

## Setup
Setup is very simple. Clone the Repo Somewhere you want:
`git@gitlab.com:susp3c1/searx-docker-localhosted.git`
Next, run your container with
`docker-compose up -d` and close it with `docker-compose donw` 
in firefox you have bypass the certificate issue under https://localhost/ and after that you are ready.

you can locate the full path of your config with
`realpath  docker-compose.yaml`
and then paste 
`docker-compose -f /wherever-you-want/docker-compose.yaml up -d ` somewhere like autostart

you can edit `searx/settings.yml` for searx default settings







